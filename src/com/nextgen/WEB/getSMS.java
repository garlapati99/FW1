package com.nextgen.WEB;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;


public class getSMS extends TearDownScript {
	
	private Properties prop;
	public String pin;
	private FileInputStream inputstream;

	public String sendPrefix(String prefix) throws FileNotFoundException, Exception {
		
		TimeUnit.SECONDS.sleep(3);
		if("chrome".equalsIgnoreCase(input))
		{
			driver2=new ChromeDriver(); 
		}
		else if("firfox".equalsIgnoreCase(input))
		{
			driver2=new FirefoxDriver();
		}else if("headless".equalsIgnoreCase(input))
		{
			driver2 = new HtmlUnitDriver(BrowserVersion.BEST_SUPPORTED,true);
		}
		
		prop = new Properties();
		inputstream = new FileInputStream("singapore.properties");
		prop.load(inputstream);
		TimeUnit.SECONDS.sleep(3);
		driver2.get(prop.getProperty("sgotp"));
		driver2.findElement(By.xpath(".//*[@id='frmOtp:searchString']")).sendKeys(prefix);
		driver2.findElement(By.xpath(".//*[@id='frmOtp']/table/tbody/tr/td[3]/input")).click();
		TimeUnit.SECONDS.sleep(2);
		String sms = driver2.findElement(By.xpath(".//*[@id='content']/table/tbody/tr/td[1]")).getText();
		System.out.println(sms);
		driver2.quit();
		return sms;
	}
}
