package com.nextgen.WEB;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;


public class Login extends TearDownScript {
	public Properties prop;
	public FileInputStream inputstream;
	
	@Test(priority=1)
	public void loginGEB() throws IOException, Exception {

		sglogger = sgreport.startTest("SG Login");
		sglogger.log(LogStatus.INFO, "SG Browser Started");
		driver.manage().window().maximize();

		prop = new Properties();
		inputstream = new FileInputStream("data.properties");
		prop.load(inputstream);
		driver.get(prop.getProperty("sgurl"));
		TimeUnit.SECONDS.sleep(2);
		
		String actual = driver.findElement(By.xpath("//*[@id='root']/div/div/div[1]/div/div/div[3]/div[2]/div/span[1]")).getText();
		String expected = "Login";
		Assert.assertEquals(expected, actual);
		
		String cssValue = driver.findElement(By.xpath("//*[@id='root']/div/div/div[1]/div/div/div[3]/div[2]/div/span[1]")).getCssValue("font-size");
		System.out.println(cssValue);
		
		
		driver.findElement(By.xpath("//*[@id='groupid']")).sendKeys(prop.getProperty("sgcompany"));
		
		
		
		
		/*
		
		driver.findElement(By.xpath(".//*[@id='company']")).sendKeys(prop.getProperty("sgcompany"));
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.xpath(".//*[@id='username']")).sendKeys(prop.getProperty("sgusername"));
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.xpath(".//*[@id='password']")).sendKeys(prop.getProperty("sgpassword"));
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.xpath(".//*[@id='login_btn']")).click();
		TimeUnit.SECONDS.sleep(2);

		try {
			if (driver.findElement(By.xpath(".//*[@id='continue_session']")).isDisplayed()) {
				TimeUnit.SECONDS.sleep(1);
				actions.moveToElement(driver.findElement(By.xpath(".//*[@id='changePasswordQA']/div[2]/span[2]"))).build().perform();
				driver.findElement(By.xpath(".//*[@id='changePasswordQA']/div[2]/span[2]")).click();
			}
		} catch (NoSuchElementException e) {
			System.out.println("No Element Found");
		}

		String serialno = prop.getProperty("sgserialno");
		String token = new getToken().sendTokenID(serialno);
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.xpath(".//*[@id='otp']")).sendKeys(token);
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.xpath(".//*[@id='otp-content']/button")).click();
		TimeUnit.SECONDS.sleep(1);
		
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='alertDialog']/div[1]")));
		//System.out.println("Test"); 
		
		if(driver.findElement(By.xpath(".//*[@id='alertDialog']/div[1]")).isDisplayed())
		{
			TimeUnit.SECONDS.sleep(1);
			driver.findElement(By.xpath(".//*[@id='okButton_label']")).click();
		}
		else
		{
			System.out.println("No pop up");
		}
		
		sglogger.log(LogStatus.PASS, "Login Successful");
		*/
	}
}