package com.nexgen.Init;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Constants {
	
	public static String timeStamp = new SimpleDateFormat("ddMMyyHHmmss").format(new Date());
	private static Constants instance = null;
	public static String WIN_EXT = ".exe";
	public static String SCREENSHOTS = "/report/screenshots/";
	public static String REPORT="/report/report"+timeStamp+".html";
	public static String CHROME_DRIVER="/lib/chromedriver";
	public static String FIREFOX_DRIVER="/lib/geckodriver";
	public static String FF_FIREBUG="/ffprofile/firebug-2.0.1.xpi";
	public static String FF_FIREPATH ="/ffprofile/firepath-0.9.7.1-fx.xpi";
	
	private Constants() { }
	
    public static Constants getInstance() {
	      if(instance == null) {
	         instance = new Constants();
	      }
	      return instance;
	}
    
    public static String getChromeDriver() {
    	String OS = System.getProperty("os.name");
    	if(OS.contains("Windows")) { return CHROME_DRIVER + WIN_EXT; }
    	else return CHROME_DRIVER;
    }
    
    public static String getFirefoxDriver() {
    	String OS = System.getProperty("os.name");
    	if(OS.contains("Windows")) { return FIREFOX_DRIVER + WIN_EXT; }
    	else return FIREFOX_DRIVER;
    }
	
	
}