package com.nextgen.WEB;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;

public class AccountServices extends TearDownScript{
	
	@Test(priority=1)
	public void acctStatement() throws Exception{
		
		TimeUnit.SECONDS.sleep(1);
		sglogger = sgreport.startTest("Account Services");
		sglogger.log(LogStatus.INFO, "Verify the Account Statement details");
		
		//actions.moveToElement(driver.findElement(By.xpath(".//*[@id='MODULE_ACCOUNT_SERVICES']"))).build().perform();
		driver.findElement(By.id(".//*[@id='MODULE_ACCOUNT_SERVICES']")).click();
		
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[contains(text(),'Account Statement')]")));
		driver.findElement(By.xpath(".//*[contains(text(),'Account Statement')]")).click();
		
		//actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dijit_MenuItem_2_text']/a"))).click().build().perform();
		TimeUnit.SECONDS.sleep(1);
		
		//driver.findElement(arg0)
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='entity_row']/a/img"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		driver.manage().timeouts().pageLoadTimeout(10,TimeUnit.SECONDS);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='sy_abbvname']")));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='sy_abbvname']")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='sy_abbvname']"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		actions.sendKeys("Entity216").build().perform();
		//driver.findElement(By.xpath(".//*[@id='sy_abbvname']")).sendKeys("Entity216");
		TimeUnit.SECONDS.sleep(2);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dijit_form_Button_0']"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='USERentities_grid']/div/div/div/div/div/div/table/tbody/tr/td[1]/a"))).build().perform();
		actions.click().build().perform();
		driver.findElement(By.xpath(".//*[@id='USERentities_grid']/div/div/div/div/div/div/table/tbody/tr/td[1]/a")).click();
		TimeUnit.SECONDS.sleep(1);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='account_no_row']/a/img"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='useraccountdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a")));
		//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(".//*[@id='useraccountdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a"))));
		driver.findElement(By.xpath(".//*[@id='useraccountdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a")).click();
		TimeUnit.SECONDS.sleep(1);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='TransactionSearchForm']/div[2]/span"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(1);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='entity']")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='entity']"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(1);
		String companyid = driver.findElement(By.xpath(".//*[@id='entity']")).getAttribute("value");
		System.out.println("Companyid>>>>>>"+ companyid);
		TimeUnit.SECONDS.sleep(1);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='entityName_row']/div"))).build().perform();
		String rescompanyid = driver.findElement(By.xpath(".//*[@id='entityName_row']/div")).getText();
		System.out.println("Response Companyid>>>>>>"+ rescompanyid);
		TimeUnit.SECONDS.sleep(1);
		Assert.assertTrue(companyid.contains(rescompanyid));
		sglogger.log(LogStatus.PASS, "Account Statement details - Pass");
	}

	@Test(priority=2)
	public void acctSummary() throws Exception {
		
		sglogger.log(LogStatus.INFO, "Verify the Account Summary details");
		TimeUnit.SECONDS.sleep(1);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='MODULE_ACCOUNT_SERVICES']"))).build().perform();
		TimeUnit.SECONDS.sleep(1);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dijit_MenuItem_1_text']"))).click().build().perform();
		TimeUnit.SECONDS.sleep(1);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='entity_row']/a/img")));
		driver.findElement(By.xpath(".//*[@id='entity_row']/a/img")).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.xpath(".//*[@id='sy_abbvname']")).sendKeys("ANGELCOMP");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.xpath(".//*[@id='dijit_form_Button_0']")).click();
		TimeUnit.SECONDS.sleep(1);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dojox_grid__View_7']/div/div/div/div/table/tbody/tr/td[1]/a")));
		driver.findElement(By.xpath(".//*[@id='dojox_grid__View_7']/div/div/div/div/table/tbody/tr/td[1]/a")).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.xpath(".//*[@id='account_no_row']/a/img")).click();
		TimeUnit.SECONDS.sleep(1);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='useraccountdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a")));
		//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(".//*[@id='useraccountdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a"))));
		driver.findElement(By.xpath(".//*[@id='useraccountdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a")).click();
		TimeUnit.SECONDS.sleep(1);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='account_no']"))).build().perform();
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='TransactionSearchForm']/div[2]/span"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dojox_grid__View_1']/div/div/div/div/table/tbody/tr[1]/td[1]/a")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dojox_grid__View_1']/div/div/div/div/table/tbody/tr[1]/td[1]/a"))).build().perform();
		String searchResults = driver.findElement(By.xpath(".//*[@id='dojox_grid__View_1']/div/div/div/div/table/tbody/tr[1]/td[1]/a")).getText();
		System.out.println(searchResults);
		TimeUnit.SECONDS.sleep(2);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='account_no']"))).click().build().perform();
		actions.sendKeys(Keys.CONTROL,Keys.HOME);
		String acctno = driver.findElement(By.xpath(".//*[@id='account_no']")).getAttribute("value");
		System.out.println(acctno);
		Assert.assertTrue(searchResults.contains(acctno));
		sglogger.log(LogStatus.PASS, "Account Summary details - Pass");
	}
}