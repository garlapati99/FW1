package com.nextgen.library;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.nexgen.Init.Constants;

public class Utility {

	public static String captureScreenshot(WebDriver driver, String Screenshotname) {
		try {
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String dest = System.getProperty("user.dir")+Constants.SCREENSHOTS+ Screenshotname + ".png";
			//String dest = "report/screenshots/" + Screenshotname + ".png";
			File destination = new File(dest);
			FileUtils.copyFile(source, destination);
			System.out.println("Screens shot captured");
			return dest;

		} catch (Exception e) {
			System.out.println("Exception while taking the screen shot " + e.getMessage());
			return e.getMessage();
		}
	}
}