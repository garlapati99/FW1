package com.nextgen.WEB;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import com.nextgen.library.Utility;
import com.relevantcodes.extentreports.LogStatus;

public class PaymentServices extends TearDownScript{
	
	@Test(priority=1)
	public void telegraphictransfer() throws Exception {
		
		TimeUnit.SECONDS.sleep(2);
		sglogger = sgreport.startTest("Payment Services");
		sglogger.log(LogStatus.INFO, "Verify the Telegraphic Transfer");
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='MODULE_CASH_SERVICES_text']"))).build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dijit_MenuItem_10_text']"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dijit__TreeNode_14']/div[1]"))).click().build().perform();
		//actions.moveToElement(driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div/div/div[2]/div/div[2]/div/div/div[3]/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/span[2]/span"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='entity_img_label']/img")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='entity_img_label']/img"))).build().perform();
		TimeUnit.SECONDS.sleep(1);
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='sy_abbvname']")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='sy_abbvname']"))).build().perform();
		TimeUnit.SECONDS.sleep(2);
		actions.click().build().perform();
		
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='USERentities_grid']/div/div/div/div/div/div/table/tbody/tr/td[1]/a")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='USERentities_grid']/div/div/div/div/div/div/table/tbody/tr/td[1]/a"))).build().perform();
		actions.click().build().perform();
		
		//actions.sendKeys("Entity216").build().perform();
		//driver.findElement(By.xpath(".//*[@id='sy_abbvname']")).sendKeys("Entity216");
		
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dijit_form_Button_0']"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='USERentities_grid']/div/div/div/div/div/div/table/tbody/tr/td[1]/a")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='USERentities_grid']/div/div/div/div/div/div/table/tbody/tr/td[1]/a"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		sglogger.log(LogStatus.INFO, "AccountList");
		String screenshot1 = Utility.captureScreenshot(driver, "Test1");
		String addScreenCapture1 = sglogger.addScreenCapture(screenshot1);
		sglogger.log(LogStatus.INFO, addScreenCapture1);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='applicant_img_label']/img"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='useraccountdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a")));
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='useraccountdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='useraccountdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='initButtons']/span")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='initButtons']/span"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='beneficiary_img_label']/img")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='beneficiary_img_label']/img"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='beneficiary_accountsdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='beneficiary_accountsdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='template_id']")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='template_id']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='template_id']")).sendKeys("Test");
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='ft_cur_code']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='ft_cur_code']")).sendKeys("SGD");
		TimeUnit.SECONDS.sleep(2);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='ft_amt']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='ft_amt']")).sendKeys("100");
		TimeUnit.SECONDS.sleep(2);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dt = new Date(System.currentTimeMillis());
		String date = sdf.format(dt);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='iss_date']"))).build().perform();
		actions.click().build().perform();
		driver.findElement(By.xpath(".//*[@id='iss_date']")).sendKeys(date);
		TimeUnit.SECONDS.sleep(3);
		actions.click().build().perform();
		actions.sendKeys(Keys.TAB);
		TimeUnit.SECONDS.sleep(2);
		
		//*[@id="iss_date"]
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='content2']/div[12]/span[3]")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='content2']/div[12]/span[3]"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		sglogger.log(LogStatus.INFO, "Submission");
		String screenshot = Utility.captureScreenshot(driver, "Test");
		String addScreenCapture = sglogger.addScreenCapture(screenshot);
		sglogger.log(LogStatus.INFO, addScreenCapture);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogButtons']/span[2]")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dialogButtons']/span[2]"))).build().perform();
		TimeUnit.SECONDS.sleep(1);
		actions.click().build().perform();
		//driver.findElement(By.xpath(".//*[@id='dialogButtons']/span[2]")).click();
		TimeUnit.SECONDS.sleep(2);
		
		try {
			if(driver.findElement(By.xpath(".//*[@id='dialogHolidayText']")).isDisplayed())
			{
				actions.moveToElement(driver.findElement(By.xpath(".//*[@id='holidayCutOffDialogButtons']/span[1]"))).build().perform();
				actions.click().build().perform();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dialogButtons']/span[2]")));
				TimeUnit.SECONDS.sleep(2);
				actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dialogButtons']/span[2]"))).build().perform();
				actions.click().build().perform();
			}
		} catch (Exception e) {
			System.out.println("No Auto Forward");
		}
		
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("es_field2_value")));
		//wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='es_field2_value']")));
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='es_field2_value']")));
		actions.moveToElement(driver.findElement(By.id("es_field2_value"))).build().perform();
		String code2 = driver.findElement(By.xpath(".//*[@id='es_field2_value']")).getText();
		System.out.println(code2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='es_field1_value']"))).build().perform();
		String code1 = driver.findElement(By.xpath(".//*[@id='es_field1_value']")).getText();
		
		String tokenid = new getToken().sendTokenID(code1, code2);
		
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='ra_otp_response']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='ra_otp_response']")).sendKeys(tokenid);
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='reauth_sb']/span"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='reauth_sb']/span")).click();
		TimeUnit.SECONDS.sleep(2);

		sglogger.log(LogStatus.PASS, "Telegraphic Transfer Passed");
	}
	
	@Test(priority=2)
	public void fastGiroCollection() throws Exception{
		TimeUnit.SECONDS.sleep(2);
		sglogger.log(LogStatus.INFO, "Verify the FAST/GIRO Collection");
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='MODULE_CASH_SERVICES_text']"))).build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dijit_MenuItem_9_text']"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dijit__TreeNode_13']/div[1]"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='entity_img_label']/img")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='entity_img_label']/img"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='sy_abbvname']")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='sy_abbvname']"))).build().perform();
		TimeUnit.SECONDS.sleep(2);
		actions.click().build().perform();
		actions.sendKeys("Entity216").build().perform();
		
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dijit_form_Button_0']"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(3);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='USERentities_grid']/div/div/div/div/div/div/table/tbody/tr/td[1]/a"))).build().perform();
		actions.click().build().perform();
		
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='applicant_img_label']/img"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='useraccountdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='useraccountdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a"))).click().build().perform();
		TimeUnit.SECONDS.sleep(1);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='initButtons']/span"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='template_id']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='template_id']")).sendKeys("Test");
		TimeUnit.SECONDS.sleep(2);

		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='beneficiary_img_label']/img"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='beneficiary_img_label']/img")).click();
		TimeUnit.SECONDS.sleep(5);
		
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(".//*[@id='beneficiary_accountsdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='beneficiary_accountsdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='beneficiary_accountsdata_grid']/div/div/div/div/div/div[1]/table/tbody/tr/td[1]/a")).click();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='ft_amt']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='ft_amt']")).sendKeys("100");
		TimeUnit.SECONDS.sleep(2);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dt = new Date(System.currentTimeMillis());
		String date = sdf.format(dt);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='iss_date']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='iss_date']")).sendKeys(date);
		TimeUnit.SECONDS.sleep(1);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='beneficiary_reference_g3']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='beneficiary_reference_g3']")).sendKeys("Test Reference");
		TimeUnit.SECONDS.sleep(1);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dda_reference']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='dda_reference']")).sendKeys("Test DDA");
		TimeUnit.SECONDS.sleep(1);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='collection_purpose']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='collection_purpose']")).sendKeys("Business Expenses");
		
		TimeUnit.SECONDS.sleep(1);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='content2']/div[10]/span[2]"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='content2']/div[10]/span[2]")).click();
		
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dialogButtons']/span[2]/span"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='dialogButtons']/span[2]/span")).click();
		
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='es_field2_value']"))).build().perform();
		String code2 = driver.findElement(By.xpath(".//*[@id='es_field2_value']")).getText();
		System.out.println(code2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='es_field1_value']"))).build().perform();
		String code1 = driver.findElement(By.xpath(".//*[@id='es_field1_value']")).getText();
		
		String tokenid = new getToken().sendTokenID(code1, code2);
		
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='ra_otp_response']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='ra_otp_response']")).sendKeys(tokenid);
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='reauth_sb']/span"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='reauth_sb']/span")).click();
		TimeUnit.SECONDS.sleep(2);

		sglogger.log(LogStatus.PASS, "FAST/GIRO Collection Passed");
	}
	
	//@Test(priority=3)
	public void  createBulkTransfer() throws Exception{
		TimeUnit.SECONDS.sleep(2);
		sglogger.log(LogStatus.INFO, "Verify the creation of the bulk transfer");
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='MODULE_CASH_SERVICES_text']"))).build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath("//*[@id='dijit_MenuItem_13_text']/a"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath("//*[@id='dijit__TreeNode_1']/div[1]/span[2]/span"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(1);
		actions.moveToElement(driver.findElement(By.xpath("//*[@id='dijit__TreeNode_6']/div[1]/span[2]/span"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(1);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='entity_img_label']/img"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='sy_abbvname']")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='sy_abbvname']"))).build().perform();
		TimeUnit.SECONDS.sleep(2);
		driver.findElement(By.xpath(".//*[@id='sy_abbvname']")).sendKeys("Entity216");
		
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='dijit_form_Button_1_label']")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dijit_form_Button_1_label']"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='USERentities_grid']/div[2]/div/div/div/div/div/table/tbody/tr/td[1]/a")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='USERentities_grid']/div[2]/div/div/div/div/div/table/tbody/tr/td[1]/a"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='widget_bk_type']/div[1]")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='widget_bk_type']/div[1]"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='bk_type_popup0']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='bk_type_popup0']")).click();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='widget_payroll_type']/div[1]"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='payroll_type_popup0']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='payroll_type_popup0']")).click();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='widget_child_sub_product_code']/div[1]"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='child_sub_product_code_popup0']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='child_sub_product_code_popup0']")).click();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='applicant_img_label']/img"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='applicant_img_label']/img")).click();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='useraccountdata_grid']/div[2]/div/div/div/div/div[1]/table/tbody/tr/td[1]/a"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='useraccountdata_grid']/div[2]/div/div/div/div/div[1]/table/tbody/tr/td[1]/a")).click();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='content1']/div/div/div[12]/span[2]/span"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='content1']/div/div/div[12]/span[2]/span")).click();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='template_id']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='template_id']")).sendKeys("Test");
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='narrative_additional_instructions']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='narrative_additional_instructions']")).sendKeys("Test Description");
		TimeUnit.SECONDS.sleep(1);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='ultimate_originating_customer']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='ultimate_originating_customer']")).sendKeys("Customer");
		TimeUnit.SECONDS.sleep(1);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='addTransactionButtonContainer']/span[1]"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='addTransactionButtonContainer']/span[1]")).click();
		TimeUnit.SECONDS.sleep(1);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='beneficiary_img_label']/img"))).click().build().perform();
		TimeUnit.SECONDS.sleep(1);
		
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(".//*[@id='beneficiary_accountsdata_grid']/div[2]/div/div/div/div/div[1]/table/tbody/tr/td[1]/a")));
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='beneficiary_accountsdata_grid']/div[2]/div/div/div/div/div[1]/table/tbody/tr/td[1]/a"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='ft_amt']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='ft_amt']")).sendKeys("100");
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='beneficiary_reference_g3']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='beneficiary_reference_g3']")).sendKeys("123456");
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='widget_payment_purpose']/div[1]"))).click().build().perform();
		TimeUnit.SECONDS.sleep(1);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='payment_purpose_popup0']"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='addBulkButton2_label']"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='dialogButtons']/span[2]"))).click().build().perform();
		TimeUnit.SECONDS.sleep(5);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='content2']/div[4]/span[2]/span")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='content2']/div[4]/span[2]/span"))).build().perform();
		actions.click().build().perform();
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='content2']/div[4]/span[2]/span")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='content2']/div[4]/span[2]/span"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='content2']/div[4]/span[2]/span")).click();
		TimeUnit.SECONDS.sleep(2);
		//driver.findElement(By.xpath(".//*[@id='content2']/div[4]/span[2]")).click();
		//actions.click().build().perform();
		
		//driver.findElement(By.xpath("//*[@id='submitBulkButton2_label']")).click();
		//actions.moveToElement(driver.findElement(By.xpath(".//*[@id='submitBulkButton2_label']"))).build().perform();
		//driver.findElement(By.xpath(".//*[@id='submitBulkButton2_label']")).click();
		//actions.click().build().perform();
		//driver.findElement(By.xpath("//*[@id='submitBulkButton2_label']")).click();
		//actions.moveToElement(driver.findElement(By.xpath(".//*[@id='submitBulkButton2_label']"))).build().perform();
		//actions.click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='okButton_label']")));
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='okButton_label']")));
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='okButton_label']"))).click().build().perform();
		TimeUnit.SECONDS.sleep(2);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='es_field2_value']"))).build().perform();
		String code2 = driver.findElement(By.xpath(".//*[@id='es_field2_value']")).getText();
		System.out.println(code2);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='es_field1_value']"))).build().perform();
		String code1 = driver.findElement(By.xpath(".//*[@id='es_field1_value']")).getText();
		String tokenid = new getToken().sendTokenID(code1, code2);
		TimeUnit.SECONDS.sleep(2);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='ra_otp_response']"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='ra_otp_response']")).sendKeys(tokenid);
		TimeUnit.SECONDS.sleep(2);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='reauth_sb']/span"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='reauth_sb']/span")).click();
		TimeUnit.SECONDS.sleep(2);
		sglogger.log(LogStatus.PASS, "Bulk Transfers Passed");
	}
	
	//@Test(priority=4)
	public void postDatedTransactions() throws Exception{
		
		//checking the javascript load time
		
		TimeUnit.SECONDS.sleep(2);
		
	}
}
