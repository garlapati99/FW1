package com.nexgen.Init;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class DriverInstance {

	public static WebDriver driver;
	public static WebDriver driver2;
	public static WebDriver driver3;
	public static WebDriver driver4;
	private static FirefoxProfile profile;
	protected Properties prop;
	private FileInputStream inputstream;
	public static String input;
	public static ExtentTest sglogger;
	
	public static ExtentReports sgreport = new ExtentReports(System.getProperty("user.dir")+Constants.REPORT);
	
	protected String exec_date;
	protected String exec_time;
	protected String scenario1;

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	Date dt = new Date(System.currentTimeMillis());
	
	protected String date = sdf.format(dt);
	protected String time = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
	public static WebDriverWait wait;
	public static Actions actions;

	@BeforeSuite
	public void driverInstance() throws IOException, InterruptedException {
		
		prop = new Properties();
		inputstream = new FileInputStream("data.properties");
		prop.load(inputstream);
		input = prop.getProperty("browser");
		System.out.println(input);
		if ("chrome".equalsIgnoreCase(input)) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+Constants.getChromeDriver());
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--lang=en-US");
            options.addArguments("--start-maximized");
            options.addArguments("--disable-web-security");
            options.addArguments("--allow-running-insecure-content");
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			driver = new ChromeDriver(options);
		} else if ("firefox".equalsIgnoreCase(input)) {
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+Constants.getFirefoxDriver());
			final String firebugpath = System.getProperty("user.dir")+Constants.FF_FIREBUG;
			final String firepath = System.getProperty("user.dir")+Constants.FF_FIREPATH;
			profile = new FirefoxProfile();
			profile.addExtension(new File(firebugpath));
			profile.addExtension(new File(firepath));
			profile.setPreference("extensions.firebug.showFirstRunPage", false);
			driver = new FirefoxDriver();
		} else if ("headless".equalsIgnoreCase(input)) {
			driver = new HtmlUnitDriver(BrowserVersion.BEST_SUPPORTED, true);
		}
		wait = new WebDriverWait(driver, 30);
		actions = new Actions(driver);
	}

	@AfterSuite
	public void closeBrowser() {
		sgreport.endTest(sglogger);
		sgreport.flush();
		//driver.get(System.getProperty("user.dir")+Constants.REPORT);
		driver.quit();
		//driver2.quit();
		//driver3.quit();
		//driver4.quit();
	}
}