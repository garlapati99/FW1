package com.nextgen.WEB;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

import com.nextgen.library.Utility;
import com.relevantcodes.extentreports.LogStatus;

public class TearDownScript extends com.nexgen.Init.DriverInstance {

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			String Screenshot_path = Utility.captureScreenshot(driver, result.getName());
			String image = sglogger.addScreenCapture(Screenshot_path);
			sglogger.log(LogStatus.FAIL, "Title Verification", image);
			sglogger.log(LogStatus.FAIL, result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			String Screenshot_path = Utility.captureScreenshot(driver, result.getName());
			String image = sglogger.addScreenCapture(Screenshot_path);
			sglogger.log(LogStatus.PASS, "Title Verification", image);
		}
		String resultStr = "";
		if (result.getStatus() == ITestResult.FAILURE) {
			resultStr = "Failed";
		} else {
			resultStr = "Passed";
		}
		System.out.println("For Testing >>>>>" + date);
		System.out.println("For Testing >>>>>" + time);
		System.out.println("For Testing >>>>>" + resultStr);
		System.out.println("For Testing >>>>>" + result.getName());
	}
}
