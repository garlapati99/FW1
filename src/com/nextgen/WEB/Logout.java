package com.nextgen.WEB;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;

public class Logout extends TearDownScript{
	
	@Test(priority=1)
	public void doLogout() throws Exception{
		
		sglogger = sgreport.startTest("Logout");
		sglogger.log(LogStatus.INFO, "Verify the Logout");

		TimeUnit.SECONDS.sleep(2);
		actions.moveToElement(driver.findElement(By.xpath(".//*[@id='layout']/div[1]/div[1]/table/tbody/tr/td[3]/div/div[2]/div/span[3]/a"))).build().perform();
		driver.findElement(By.xpath(".//*[@id='layout']/div[1]/div[1]/table/tbody/tr/td[3]/div/div[2]/div/span[3]/a")).click();
		
		sglogger.log(LogStatus.PASS, "Logout Successful");
		
	}

}
