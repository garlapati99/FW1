package com.nextgen.WEB;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;


	public class getToken extends TearDownScript {
		
		private Properties prop;
		public String pin;
		private FileInputStream inputstream;
		
		
		public String sendTokenID(String serialno) throws FileNotFoundException, Exception
		{
			
			TimeUnit.SECONDS.sleep(3);
			if("chrome".equalsIgnoreCase(input))
			{
				driver3=new ChromeDriver(); 
			}
			else if("firefox".equalsIgnoreCase(input))
			{
				driver3=new FirefoxDriver();
			}else if("headless".equalsIgnoreCase(input))
			{
				driver3 = new HtmlUnitDriver(BrowserVersion.BEST_SUPPORTED,true);
			}
			
			prop = new Properties();
			inputstream = new FileInputStream("data.properties");
			prop.load(inputstream);
			TimeUnit.SECONDS.sleep(3);
			driver3.get(prop.getProperty("sgtokenurl"));
			TimeUnit.SECONDS.sleep(3);
			driver3.findElement(By.xpath(".//input[@id='pibid']")).sendKeys(serialno);
			driver3.findElement(By.xpath(".//*[@id='submitOTP']")).click();
			TimeUnit.SECONDS.sleep(3);
			String otp = driver3.findElement(By.xpath("//input[@id='generatedOtp']")).getAttribute("value");
			System.out.println(otp);
			driver3.quit();
			return otp;	
		}
		
		public String sendTokenID(String code1, String code2) throws Exception
		{
			TimeUnit.SECONDS.sleep(3);
			if("chrome".equalsIgnoreCase(input))
			{
				driver3=new ChromeDriver(); 
			}
			else if("firfox".equalsIgnoreCase(input))
			{
				driver3=new FirefoxDriver();
			}else if("headless".equalsIgnoreCase(input))
			{
				driver3 = new HtmlUnitDriver(BrowserVersion.BEST_SUPPORTED,true);
			}
			
			prop = new Properties();
			inputstream = new FileInputStream("data.properties");
			prop.load(inputstream);
			TimeUnit.SECONDS.sleep(3);
			driver3.get(prop.getProperty("sgtokenurl"));
			TimeUnit.SECONDS.sleep(3);
			driver3.findElement(By.xpath(".//input[@id='pibid']")).sendKeys(prop.getProperty("sgserialno"));
			driver3.findElement(By.xpath(".//*[@id='data1']")).sendKeys(code1);
			driver3.findElement(By.xpath(".//*[@id='data2']")).sendKeys(code2);
			driver3.findElement(By.xpath(".//*[@value='Generate E-Sign 2']")).click();
			//driver3.findElement(By.xpath(".//*[@id='submitESign']")).click();
			TimeUnit.SECONDS.sleep(3);
			String otp = driver3.findElement(By.xpath("//input[@id='generatedOtp']")).getAttribute("value");
			System.out.println(otp);
			driver3.quit();
			return otp;
			
		}
}
